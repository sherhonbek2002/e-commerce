from django.urls import path
from .views import HomeView,ProductDetail,ProductListView,CartView,LoginView,LogoutView,UserProfileView,register,CartItemDeleteView,SearchView,OrderListView,OrderDetail,CheckOutView,CategoryView,CategoryProducts

urlpatterns = [
    path('', HomeView.as_view(),name="home"),

    path('products/',ProductListView.as_view(), name='products_list'),

    path('products/search',ProductListView.as_view(), name='search'),

    path("products/<int:pk>", ProductDetail.as_view(),name="product_detail"),

    path("cart/",CartView.as_view(),name="cart"),

    path("checkout",CheckOutView.as_view(), name="checkout"),

    path("cart/delete/<int:pk>",CartItemDeleteView.as_view(),name="cart_item_delete"),

    path("user/login",LoginView.as_view(),name='login'),

    path("user/register/",register,name='register'),

    path("user/logout",LogoutView.as_view(),name='logout'),

    path("user/profile",UserProfileView.as_view(),name='profile'),


    path('category/', CategoryView.as_view(),name='category'),

    path('categories/<slug:slug>',CategoryProducts.as_view(),name='sorted_by_category'),

    path("orders/",OrderListView.as_view(), name='orders'),

    path('orders/<int:pk>', OrderDetail.as_view(), name = 'order_detail')



]