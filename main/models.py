from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class Category(models.Model):
    name = models.CharField(max_length=250,unique=True)
    slug = models.SlugField(max_length=300,unique=True)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

class Brand(models.Model):
    name = models.CharField(max_length=250)
    image = models.ImageField(upload_to="brands_images/")
    slug = models.SlugField(max_length=300,unique=True)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = "Brand"
        verbose_name_plural = "Brands"

class Product(models.Model):
    full_name = models.CharField(max_length=250)
    description = models.TextField(max_length=1500,)
    company = models.ForeignKey(Brand,on_delete=models.SET_NULL,null=True,related_name='products',blank=True)
    image = models.ImageField(upload_to="products/images/")
    category = models.ForeignKey(Category,on_delete=models.SET_NULL, null=True,related_name='products',)
    price = models.FloatField()
    created_date = models.DateTimeField(auto_now_add=True)
    count_sold = models.IntegerField(default=0,null=True)
    def __str__(self):
        return self.full_name
    class Meta:
        verbose_name = 'product'
        verbose_name_plural = 'products'

class Cart(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE,related_name='carts',blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    is_sold = models.BooleanField(default=False)



class CartItem(models.Model):

    product = models.ForeignKey(Product,on_delete=models.CASCADE,related_name='cart_items')
    quantity = models.PositiveIntegerField()
    price = models.FloatField()
    cart = models.ForeignKey(Cart,on_delete=models.CASCADE,related_name="cart_items")


class ProductAlbum(models.Model):
    image = models.ImageField(upload_to='products/products_albums/')
    product = models.ForeignKey(Product,on_delete=models.CASCADE, related_name='album')
    def __str__(self):
        return self.product.full_name + " | image"

class SocialNetworks(models.Model):
    ICONS = (
        ("fa fa-telegram",'telegram'),
        ("fa fa-facebook",'facebook'),
        ("fa fa-instagram",'instagram'),
        ("fa fa-youtube",'youtube'),
        ("fa fa-quora",'quora'),

    )
    name = models.CharField(max_length=200,unique=True)
    icon = models.CharField(max_length=50,choices=ICONS,unique=True)
    url = models.URLField()

class Anon(models.Model):
    image = models.ImageField(upload_to="anons/images/")
    title = models.CharField(max_length=300)
    subtitle = models.CharField(max_length=300)

class Profile(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE, related_name='profile',)
    country = models.CharField(max_length=200)
    city_or_province = models.CharField(max_length=250)
    region = models.CharField(max_length=250)
    home_address = models.CharField(max_length=300)
    post_code = models.CharField(max_length=20,blank=True)
    phone_number = models.CharField(max_length=15,blank=True)
    full_name = models.CharField(max_length=300,blank=True)

class SalesOffice(models.Model):
    country = models.CharField(max_length=200)
    city_or_province = models.CharField(max_length=250, blank=True)
    region = models.CharField(max_length=250, blank=True)
    google_map_url = models.URLField()

class Vendor(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE,related_name='vendors')
    sales_office = models.ForeignKey(SalesOffice,on_delete=models.CASCADE,related_name='vendors')
    is_vendor_of_city = models.BooleanField(default=False)
    is_header_vendor = models.BooleanField(default=False)

class Order(models.Model):
    vendor = models.ForeignKey(Vendor,on_delete=models.SET_NULL, null=True,related_name='orders')
    cart = models.OneToOneField(Cart, related_name='order', on_delete=models.CASCADE)
    country = models.CharField(max_length=200)
    city_or_province = models.CharField(max_length=250)
    region = models.CharField(max_length=250)
    home_address = models.CharField(max_length=300)
    post_code = models.CharField(max_length=20)
    phone_number = models.CharField(max_length=15)
    full_name = models.CharField(max_length=2500)
    is_sold = models.BooleanField(default=False)






