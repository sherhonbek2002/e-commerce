from .models import Cart,Product,CartItem
from django.shortcuts import redirect
from random import choice
def visitor_ip_address(request):

    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')

    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')

    return ip

def get_cart(request)->Cart:
    if request.user.is_authenticated:
        try:
            cart = Cart.objects.get(user=request.user, is_sold=False)
        except Cart.DoesNotExist:
            cart = Cart.objects.create(user=request.user)
        return cart
    else:
        return None

def delete_cart_item(request,pk):

    try:
        cart = Cart.objects.get(user=request.user,is_sold=False)
    except Cart.DoesNotExist:
        return False
    try:
        cart_item = cart.cart_items.get(pk=pk)
    except Cart.DoesNotExist:
        return False
    print(cart_item)
    cart_item.delete()
    return True
def calculate_price(cart:Cart):
    s = 0
    try:
        for cart_item in cart.cart_items.all():
            s += cart_item.price
    except:
        pass
    return s
def product_in_cart(request,product:Product):
    if request.user.is_authenticated:
        try:
            cart = Cart.objects.filter(is_sold=False, user=request.user).first()
            if cart:
                if cart.cart_items.filter(product=product).count() > 0:
                    return cart.cart_items.filter(product=product)
        except Cart.DoesNotExist:
            return False
    return False

def cart_info(request):
    cart = get_cart(request)
    quantity = 0
    try:
        quantity = cart.cart_items.all().count()
    except:
        pass
    total_price = calculate_price(cart)
    my_dict = {
        'quantity':quantity,
        'total_price':total_price,
    }
    return my_dict

def random_products(products):
    r_products = []
    try:
        if (not products) or (products.count() < 4):
            return products
        while len(r_products) < 4:
            product = choice(products)
            if product not in r_products:
                r_products.append(product)
    except:
        pass


    return r_products
def top_sellers():
    products = Product.objects.order_by('count_sold')
    try:
        if products.count()>3:
            products = products[:3]
    except:
        pass
    return products



