from django.shortcuts import render,redirect
from random import choice
from django.views import View
from django.views.generic.list import ListView
from .models import Category,Product,Cart,CartItem,ProductAlbum,Brand,Anon,SocialNetworks,Profile,Order,Vendor,SalesOffice
from django.http import Http404
from django.contrib.auth import authenticate,login,logout
from .helper import visitor_ip_address,get_cart, delete_cart_item,calculate_price,product_in_cart,cart_info,random_products,top_sellers
from .forms import RegisterForm,ProfileForm
from django.contrib.auth import get_user_model

User = get_user_model()

social_networks = SocialNetworks.objects.all()
categories = Category.objects.all()
recent_posts = Product.objects.all()[:5]
top_sellers = top_sellers()
products = Product.objects.all()
random_products = random_products(products)

class HomeView(View):
    def get(self,request):
        print(categories)
        anons = Anon.objects.all()
        try:
            if anons.count()>4:
                anons = anons[:4]
        except:
            pass

        brands = Brand.objects.all()
        try:
            top_new = products[:3]
        except:
            top_new = None


        return render(request,'main/index.html',context={
            "categories":categories,
            "products":products,
            'brands':brands,
            'anons':anons,
            'cart_info':cart_info(request),
            'top_sellers':top_sellers,
            'recent_posts':recent_posts,
            'social_networks':social_networks,
            'top_new':top_new,

        })

class ProductListView(ListView):
    model = Product
    queryset = products
    context_object_name = 'products'
    template_name = "main/shop.html"
    paginate_by = 8
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = categories
        context['social_networks'] = social_networks
        context['cart_info'] = cart_info(self.request)

        return context
    def get_queryset(self):
        search_word = self.request.GET.get('search_word',None)
        object_list = Product.objects.all()
        if search_word:
            object_list = object_list.filter(full_name__icontains=search_word)
        return object_list


class ProductDetail(View):

    def get(self,request,pk=0):

        categories = Category.objects.all()

        try:
            product = Product.objects.get(id=pk)
        except Product.DoesNotExist:
            if not products:
                return redirect('products_list')
            product = choice(products)
            return redirect('product_detail', pk=product.id)

        related_products = Product.objects.filter(category=product.category)

        album = ProductAlbum.objects.filter(product=product)
        print(random_products)

        return render(request, "main/single-product.html", context={
            'product': product,
            'categories': categories,
            'related_products': related_products,
            'recent_posts': recent_posts,
            'products': random_products,
            'album': album,
            'product_in_cart': product_in_cart(request, product),
            'social_networks': social_networks,
            'cart_info': cart_info(request),

        })

    def post(self,request,pk=0):
        if request.user.is_authenticated:
            try:
                product = Product.objects.get(id=pk)
            except Product.DoesNotExist:
                return Http404()
            quantity = request.POST.get('quantity',None)

            cart = get_cart(request)
            print(cart)
            if cart.cart_items.filter(product=product).count()>0:
                return redirect('cart')
            cart_item = CartItem.objects.create(cart=cart,quantity=quantity,product=product,price=float(quantity)*float(product.price))

            return redirect('product_detail',pk=product.id)
        else:
            return redirect('login')

class CategoryView(View):
    def get(self,request):
        return render(request, 'main/category.html', context={
            'categories':categories,
            'cart_info':cart_info(request),
            'products':random_products,
        })
class CategoryProducts(View):
    def get(self,request,slug):
        category = None
        try:
            category = Category.objects.get(slug=slug)
            products = category.products.all()
        except Category.DoesNotExist:
            products = []

        return render(request,'main/shop.html', context={
            'categories':categories,
            'products':products,
        })


class UserProfileView(View):
    def get(self,request):
        if request.user.is_authenticated:
            cart_history = request.user.carts.filter(is_sold=True)

            profile = Profile.objects.get(user=request.user)
            return render(request,'main/profile.html', context={
                'profile':profile,
                'cart_history':cart_history,
                'cart_info': cart_info(request),
            })
        else:
            return redirect('login')
    def post(self,request):
        if request.user.is_authenticated:
            full_name = request.POST.get('full_name',None)
            city_or_province = request.POST.get('city_or_province',None)
            region = request.POST.get('region',None)
            home_address = request.POST.get('home_address',None)
            post_code = request.POST.get('post_code',None)
            phone_number = request.POST.get('phone_number',None)
            profile = Profile.objects.get(user=request.user)
            if full_name:
                profile.full_name = full_name
            if home_address:
                profile.address = home_address
            if post_code:
                profile.post_code = post_code
            if phone_number:
                profile.phone_number = phone_number
            if city_or_province:
                profile.city_or_province = city_or_province
            if region:
                profile.region = region
            profile.save()
            return redirect('profile')
        else:
            return redirect('login')




class LoginView(View):
    def get(self,request):
        if not request.user.is_authenticated:
            return render(request,"main/login.html", context={

                'categories':categories,
                'cart_info':cart_info(request),
                'social_networks':social_networks
            })
        else:
            return redirect('products_list')
    def post(self,request):
        if not request.user.is_authenticated:
            username = request.POST.get('username',None)
            password = request.POST.get('password',None)
            if not (username and  password):
                return render(request,"main/login.html", context={
                    "message":"Please write all",
                    'categories': categories,
                    'cart_info': cart_info(request),
                    'social_networks':social_networks
                })
            user = authenticate(username=username, password=password)
            if not user:
                return render(request, "main/login.html", context={
                    "message": "Email or password wrong!",
                    'categories': categories,
                    'cart_info': cart_info(request),
                    'social_networks':social_networks
                })
            login(request,user)
            return redirect('home')
        else:
            return redirect('products_list')


def register(request):
    if not request.user.is_authenticated:
        if request.method == "POST":
            form = RegisterForm(request.POST)
            if form.is_valid():
                password1 = form.cleaned_data.get('password1')
                username = form.cleaned_data.get('username')

                form.save()
                user = authenticate(username=username, password=password1)

                login(request,user)
                return redirect('home')
            else:
                return render(request,'main/register.html',context={
                    'form':form,
                    'social_networks':social_networks,
                    'categories': categories,
                    'cart_info': cart_info(request),
                })


        return render(request,'main/register.html',context={
                'categories': categories,
                'cart_info':cart_info(request),


            })
    return redirect('home')


class CartView(View):
    def get(self,request):
        if request.user.is_authenticated:
            cart = get_cart(request)
            price_total = calculate_price(cart)
            interested_products = []
            if products and products.count() > 1:
                while len(interested_products) < 3:
                    product = choice(products)
                    if product not in interested_products:
                        interested_products.append(product)
            return render(request,"main/cart.html", context={
                "cart":cart,
                "price":price_total,
                'products':random_products,
                'social_networks':social_networks,
                'categories':categories,
                'cart_info':cart_info(request),
                'recent_posts':recent_posts,
                'interested_products':interested_products,

            })
        else:
            return redirect('login')


class CartItemDeleteView(View):
    def get(self,request,pk):
        if pk:
            if delete_cart_item(request,pk):
                return redirect('cart')

        return redirect('cart')




class LogoutView(View):
    def get(self,request):
        logout(request)
        return redirect('home')

class ProfileView(View):
    def get(self,request):
        if request.user.is_authenticated:
            carts = Cart.objects.filter(user=request.user,is_sold=True)
            profile  = Profile.objects.get(user=request.user)
            form = ProfileForm()

            return render(request,"main/profile.html" , context={
                'carts':carts,
                'profile':profile,
                'categories':categories,
                'social_networks':social_networks,
                'form':form,
                'cart_info': cart_info(request),

            })
        return redirect('register')
    def post(self,request):
        if request.user.is_authenticated:

            user = request.user
            form = ProfileForm(request.POST)
            if form.is_valid():
                form.save(user)
            redirect('profile')



        return redirect('register')




class SearchView(View):
    def get(self,request):
        return redirect('products_list')

class CheckOutView(View):
    def get(self,request):
        if request.user.is_authenticated:
            profile = Profile.objects.get(user=request.user)
            cart = get_cart(request)
            salesoffices = SalesOffice.objects.all()
            return render(request,'main/checkout.html',context={
                'cart_info': cart_info(request),
                'cart': cart,
                'products': products,
                'recent_posts': recent_posts,
                'categories': categories,
                'social_networks':social_networks,
                'profile':profile,
                'salesoffices':salesoffices
            })
        else:
            return redirect('login')
    def post(self,request):
        if request.user.is_authenticated:
            user = request.user

            profile = Profile.objects.get(user=user)

            if profile.full_name and profile.city_or_province and profile.region and profile.home_address and profile.post_code and profile.phone_number and profile.country:
                cart = get_cart(request)
                cart.is_sold = True
                cart.save()
                Order.objects.create(cart=cart,full_name=profile.full_name,phone_number=profile.phone_number,post_code=profile.post_code,city_or_province=profile.city_or_province,region=profile.region,home_address=profile.home_address,country=profile.country)
                return render(request, 'main/checkout.html', context={
                    'cart_info': cart_info(request),
                    'cart': cart,
                    'products': products,
                    'recent_posts': recent_posts,
                    'categories': categories,
                    'social_networks': social_networks
                })
            else:
                return redirect('profile')
        else:
            return redirect('login')




class OrderListView(View):
    def get(self,request):
        if request.user.is_authenticated:
            try:
                vendor = Vendor.objects.get(user=request.user)

                city_or_province = vendor.city_or_province
                orders = Order.objects.filter(is_sold=False, city_or_province=city_or_province)
            except Vendor.DoesNotExist:
                redirect('home')



            return render(request,'main/orders.html',context={
                'orders':orders,
            })
        else:
            return redirect('home')
    def post(self,request):
        pass
class OrderDetail(View):
    def post(self,request,pk):

        if request.user.is_authenticated:
            print(request.POST)
            try:
                order = Order.objects.get(pk=pk)
                if request.POST.get('delete',None):
                    order.delete()
                elif request.POST.get('create',None):
                    order.is_sold = True
                    order.save()
                else:
                    redirect('order_detail', pk=pk)

            except Order.DoesNotExist:
                pass

            return redirect('orders')
        else:
            return redirect('home')




