from django.db.models.signals import post_save,post_delete
from django.dispatch import receiver
from .models import Cart,CartItem,User,Profile


@receiver(post_save,sender=CartItem)
def add_count(sender,instance,created, **kwargs):
    if created:
        a = instance.product.count_sold
        a = a + 1
        instance.product.count_sold = a
        instance.product.save()
@receiver(post_delete,sender=CartItem)
def minus_count(instance,**kwargs):
    a = instance.product.count_sold
    a = a - 1
    instance.product.count_sold = a
    instance.product.save()

@receiver(post_save,sender=User)
def create_profile(sender,instance,**kwargs):
    if Profile.objects.filter(user=instance).count() == 0:
        Profile.objects.create(user=instance)