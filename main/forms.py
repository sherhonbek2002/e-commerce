from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from main.models import Profile,Cart,Order

User = get_user_model()

class RegisterForm(UserCreationForm):
    username = forms.CharField(max_length=250)
    email = forms.EmailField()
    class Meta:
        model = User
        fields = ('username','email','password1','password2')
class ProfileForm(forms.Form):
    city_or_province = forms.CharField(max_length=250)
    region = forms.CharField(max_length=250)
    home_address = forms.CharField(max_length=300)
    post_code = forms.CharField(max_length=20)
    phone_number = forms.CharField(max_length=15)
    full_name = forms.CharField(max_length=300)
    def save(self,user:User):
        profile = Profile.objects.get(user=user)
        profile.objects.update(self.cleaned_data)
        return profile
class OrderForm(forms.Form):
    country = forms.CharField(max_length=200)
    city_or_province = forms.CharField(max_length=250)
    region = forms.CharField(max_length=250)
    home_address = forms.CharField(max_length=300)
    post_code = forms.CharField(max_length=20)
    phone_number = forms.CharField(max_length=15)
    full_name = forms.CharField(max_length=300)
    def save(self,cart:Cart):
        order = Order.objects.create(cart=cart,city_or_province=self.country,)











