from django.contrib import admin
from .models import Category,Product,Brand,ProductAlbum,CartItem,Cart,Anon,SocialNetworks,Profile,Vendor,Order,SalesOffice

admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Brand)
admin.site.register(ProductAlbum)
admin.site.register(CartItem)
admin.site.register(Cart)
admin.site.register(Anon)
admin.site.register(SocialNetworks)
admin.site.register(Profile)
admin.site.register(Order)
admin.site.register(Vendor)
admin.site.register(SalesOffice)
